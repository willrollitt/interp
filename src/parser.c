#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "parser.h"

/* Trims whitespace from a string */
char *parser_trim_whitespace (char *str) {
	char *result;

	while (isspace((unsigned char) *str)) str++;

	if (*str)
		return str; /* All whitespace */

	/* Trim whitespace */
	result = str + strlen(str) - 1;
	while (result > str && isspace((unsigned char)*result)) result--;

	result[1] = '\0';

	return str;
}

/* Returns char * to first word in string and trims whitespace */
char *parser_get_cmd (const char *line, const char delim, ssize_t len) {
	char *cmd = (char*)malloc(len);

	do {
		strncat(cmd, *&line, 1);
	} while (*++line != delim);

	cmd = parser_trim_whitespace(cmd);
	return cmd;
}

ssize_t parser_is_cmd (const char *cmd) {
	for (size_t i = 0; cmd_map[i].str; i++)
		if (!strncmp(cmd_map[i].str, cmd, strlen(cmd_map[i].str)))
			return i;

	fprintf(stderr, "-E No such function %s\n", cmd);
	return -1;
}

int parser_read_line (const char *line, const size_t len) {
	char *cmd;
	ssize_t cmd_index = 0;

	/* Get the instruction/command */
	cmd = parser_get_cmd(line, ':', len);

	if ((cmd_index = parser_is_cmd(cmd)) == -1)
		return 1;

	printf("%s\n", cmd);

	memset(cmd, 0, strlen(cmd));
	free(cmd);
	return 0;
}
